function ContentBlockExtendIt(This){
    var _This = This;
    var Top = eval(_This.position().top)-5;
    var Height = eval($(window).height());
    var ToolbarHeight = eval($('.toolbar').height());
    ToolbarHeight = 0;
    var NewHeight = Height-(ToolbarHeight+Top);
    _This.height(NewHeight);
}

function ExtendIt(This){
    var _This = This;
    var Top = eval(_This.position().top);
    var Height = eval($(window).height());
    var ToolbarHeight = eval($('.toolbar').height());
    ToolbarHeight = 0;
    var NewHeight = Height-(ToolbarHeight+Top);
    _This.height(NewHeight);
    console.log(Top + ' ' + Height + ' ' + ToolbarHeight + ' ' + NewHeight)
}

function OpenInMapsLinks(){
    if(myApp.device.os == 'ios'){
        var OpenInMapsurl = "maps://maps.apple.com/?daddr=16 Langford Road,Westville,3629&dirflg=d";
    }else if(myApp.device.os == 'android'){
        var OpenInMapsurl = "https://www.google.co.za/maps/place/16+Langford+Rd,Westville,3629";
    }
    return OpenInMapsurl;
}

function FindFields(formname){
    var fields = "_token=" + $('input[name="_token"]').val();
    $('#' + formname + ' input,#' + formname + ' textarea,#' + formname + ' select').each(function(k){
        if($(this).attr('name') != '_token'){
            fields += '&' + $(this).attr('name') + '=' + $(this).val();
        }
    });
    return fields;
}

function validation( formname,  data ){
    $validated = 0;
    $('.alert-danger').remove();
    $formname = formname;
    /* validation */
    $('#' + formname + ' input,#' + formname + ' textarea,#' + formname + ' select').filter('[required]:visible').each(function(k, requiredField){
        $(this).css('border', '1px #ccc solid');
        var requiredcheck = $(this).attr('required');
        if (typeof requiredcheck !== typeof undefined && requiredcheck !== false) { 
            $error_msg = $(this).attr('data-required');
            if($(this).attr('type') == 'email'){
                var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                if(pattern.test($(this).val())){
                    if($formname){
                        if($formname == 'registration'){
                            $token = $('input[name="_token"]').val();
                            if(checkuser($(this).val(), $token)){
                                $value = '';
                                $validated++;
                            }
                        }
                    }
                    $value = 'selected';
                }else{
                    $value = '';
                    $validated++;
                }
            }
            else if($(this).attr('type') == 'radio'){
                if($(this).is(':checked')){
                    $value = 'selected';
                }else{
                    $value = '';
                    $validated++;
                }
            }
            else if($(this).attr('type') == 'password'){
                if($(this).val().length < 5){
                    $error_msg = 'Please enter at least 5 characters';
                    $value = '';
                    $validated++;
                }else if($(this).val().length >= 5){
                    $value = 'true';
                }

                if($(this).attr('data-type') == 'login'){
                    $error_msg = $(this).attr('data-required');
                }

                if($(this).attr('data-copy')){
                    if($(this).val() == $('#' + $(this).attr('data-copy')).val()){
                        $value = 'true';
                    }else{
                        $error_msg = 'This is not the same as your password.';
                        $value = '';
                        $validated++;
                    }
                }   
            }else if($(this).attr('type') == 'select'){
                if($(this).val()){
                    $value = 'selected';
                }else{
                    $value = '';
                    $validated++;
                }
            }else{
                $value = $(this).val().replace(/ /g,'');
            }

            if(!$value){
                $(this).closest('button').css('background-color','#fff').css('color','#fff');
                $(this).css('border', '2px solid #b71318');
                $(this).addClass('warning-input').attr('placeholder','*' + $error_msg);
                $validated++;
            }else{}
        }
    });

        /* select */
        $('#' + formname + ' select').each(function(){
            $(this).css('border', '1px #ccc solid');
            if($(this).attr('required')){   
                $error_msg = $(this).attr('data-required');
                $value = $(this).val().replace(/ /g,'');
                if(!$value){
                    $(this).css('border', '2px solid #b71318');
                    $(this).addClass('warning-input');
                    $validated++;
                }else{}
            }

        });
        /* select */    

    /* validation */
    if ($validated > 0) {
        return false;
    } else {
        return true;
    }
}

function preloader(msg, which){
    preloaderRemove();
    $(which).append($('<div class="overlay_preloader"><img src="img/preloader.svg"/><p>' + msg + '</p></div>').show().css({'opacity':0}).animate({'opacity':1}));
}

function preloaderRemove(){
    $('.overlay_preloader').fadeOut(800, function(){
        $('.overlay_preloader').remove();
    });
    
}