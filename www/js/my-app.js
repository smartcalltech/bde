var myApp = new Framework7();
var CoOrdsX = '-29.837804';
var CoOrdsY = '30.9129848';

var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true
});

// Handle Cordova Device Ready Event
$$(document).on('deviceready', function() {
    console.log("Device is ready!");
    $('.content-block.extend-it').each(function(){
        ContentBlockExtendIt($(this));
    });
    $('.secondary.extend-it').each(function(){
        ExtendIt($(this));
    });

    $('#signinform button').on('click',function(){
        var formname = 'signinform';
        //if(validation(formname)){
            preloader('', '.user-details');
            
        //}
        return false;
    });

});


// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('about', function (page) {
    // Do something here for "about" page

})

// Option 2. Using one 'pageInit' event handler for all pages:
$$(document).on('pageInit', function (e) {
    // Get page data from event data
    var page = e.detail.page;

    if (page.name === 'about') {
        // Following code will be executed for page with data-page attribute equal to "about"
        // myApp.alert('Here comes About page');
    }

    if (page.name === 'schedule-a-consult') {

    }

    if (page.name === 'find-an-attorney') {
        $$.getJSON('http://groot.dedicated.co.za/bdeattorneys/', function (data) {
          console.log(data);
            var listHTML = '<ul>';
            $.each(data, function(i,val){
                listHTML += '<li>' + i + ' ' + val['Address'] + '</li>';

            });
            listHTML += '</ul>';
            // And insert generated list to page content
            $$(page.container).find('.page-content').append(listHTML);
        });
    }

    if(page.name === 'find-our-offices'){
        var map;
        map = new GMaps({
            el: '#map',
            lat: CoOrdsX,
            lng: CoOrdsY,
            zoomControl : true,
            zoomControlOpt: {
                style : 'SMALL',
                position: 'TOP_LEFT'
            },
            panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false
        });

        map.addMarker({
          lat: CoOrdsX,
          lng: CoOrdsY,
          title: 'BDE Attorneys',
        });

        var styles = [
            {
              stylers: [
                { hue: "#364353" },
                { saturation: 0 }
              ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    { lightness: 100 },
                    { visibility: "simplified" }
              ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    { visibility: "on" }
              ]
            }
        ];

        map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"  
        });
        
        map.setStyle("map_style");

        $('#map').each(function(){
            ExtendIt($(this));
        });        
    }

    $('.content-block.extend-it').each(function(){
        ContentBlockExtendIt($(this));
    });
    $('.secondary.extend-it').each(function(){
        ExtendIt($(this));
    });
})

// Option 2. Using live 'pageInit' event handlers for each page
$$(document).on('pageInit', '.page[data-page="about"]', function (e) {
    // Following code will be executed for page with data-page attribute equal to "about"
    // myApp.alert('Here comes About page');
})